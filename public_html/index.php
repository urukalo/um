<?php

    define("PUBDIR", dirname(__FILE__) . "/");
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    
    //autoloader
    $loader = require '../vendor/autoload.php';
    $loader->add('umCore\\', 'umCore/');

    //urukalo domain mount
    $loader->add('urukalo\\', 'umApps/urukalo.com/');
    $urukalo = umCore\um::mount('urukalo', 'urukalo.com');

    //test domain mount
    $loader->add('test\\', 'umApps/test.com/');
    $test = umCore\um::mount('test', 'test.com');
    
    //um domain mount -- default for localhost
    $loader->add('um\\', 'umApps/um/');
    $um = umCore\um::mount("localhost", "um", function(){return true;});
?>