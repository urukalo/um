<?php

class controllers_index extends Controller {

    public function init() {
        $this->setLevel("index", LEVELS::GET);
        $this->Widget["home"] = new Widget("home");
    }

    function indexAction() {
        
        
        
        $model = $this->loadModel("index");
        $data['naslov'] = $model->indexModel();

        //load views
        $this->loadView("header", $data);
        $this->loadView("index", "");
        $this->loadView("footer", "");
    }

    function aboutAction(){
        
        $this->Widget["home"]->setView("about");
        $this->indexAction();
        
    }
    function howtoAction(){
        
        $this->Widget["home"]->setView("howto");
        $this->indexAction();
        
    }
    function contactAction(){
        
        $this->Widget["home"]->setView("contact");
        $this->indexAction();
        
    }
}

?>
