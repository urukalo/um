<?php

namespace umCore;

/**
 * Description of um
 *
 * @author Milan Urukalo
 */
class um extends Core {

    static public $sites = array();

    private function __construct($folder) {
        $this->core_config = require "../umApps/" . $folder . '/config/core_conf.php';
        $this->app_config = require '../umApps/' . $folder . '/config/appconf.php';

        $this->bootstrap();
    }

    static function mount($domain = 'localhost', $folder = 'um', $match = \FALSE) {
        if ($match === \FALSE) {
            $match = function () use ($domain){
                return ($_SERVER['SERVER_NAME'] == $domain);
            };
        }
        if ((!self::getSite($domain) ||!self::getSite($folder)) && ( $match ? $match() : '')) {
            self::$sites[$domain] = self::$sites[$folder] = new um($folder);
            exit();
        }
    }

    static function getSite($site) {
        return isset(self::$sites[$site]) ? self::$sites[$site] : false;
    }

    private function bootstrap() {

        echo $this->core_config;
    }

}
